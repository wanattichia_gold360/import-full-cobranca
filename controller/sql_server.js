var mssql = require('mssql');
var String = require('../apoio/string');
var Datetime = require('../apoio/datetime');

var sql = {

    select: function (call) {

        mssql.connect('mssql://sistema:vidaloka@GLD003/gold360?requestTimeout=120000').then(function() {

            new mssql.Request().query('select top 250000 * from tb_subscription_full where processado = 0').then(function(recordset) {

                call(recordset);
            }).catch(function(err) {
                console.log('erro request');
                console.log(JSON.stringify(err));
            });
        }).catch(function(err) {
            console.log('erro connect');
            console.log(JSON.stringify(err));
        });
    },

    update: function (call) {
        mssql.connect('mssql://sistema:vidaloka@GLD003/gold360?requestTimeout=120000').then(function() {

            new mssql.Request().query('update top (250000) tb_subscription_full set processado = 1 where processado = 0').then(function(err, recordset, affected) {

                console.log(affected + ' updates');

                call(!String.isNullOrEmpty(affected));
            }).catch(function(err) {
                console.log('erro request');
                console.log(JSON.stringify(err));
            });
        }).catch(function(err) {
            console.log('erro connect');
            console.log(JSON.stringify(err));
        });
    }
};

module.exports = {
    select: sql.select,
    update: sql.update
};