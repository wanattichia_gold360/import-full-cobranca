module.exports = {
    
    larger: function (arr, value) {
        var retorno = [];

        value = String.isNullOrEmpty(value) ? 0 : new Number(value);

        for (var i = 0; i < arr.length; i++){
            if (arr[i].time > value)
                retorno[retorno.length] = arr[i];
        }

        return retorno;
    }
};